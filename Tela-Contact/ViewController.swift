//
//  ViewController.swift
//  Tela-Contact
//
//  Created by COTEMIG on 14/01/1444 AH.
//

import UIKit

struct Contato {
    let label1: String
    let labelPhone: String
    let labelEmail: String
    let labelAddress: String
}
class ViewController: UIViewController, UITableViewDataSource {
    var listaDeContatos:[Contato] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as!MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.label1.text = contato.label1
        cell.labelPhone.text = contato.labelPhone
        cell.labelEmail.text = contato.labelEmail
        cell.labelAddress.text = contato.labelAddress
        
        return cell
        
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        tableView.dataSource = self
        tableView.delegate = self
        
        
        listaDeContatos.append(Contato(label1: "Contato 1", labelPhone: "31 98321-2345", labelEmail: "contact1@gmail.com", labelAddress: "Rua do Cristal, 11"))
        listaDeContatos.append(Contato(label1: "Contato 2", labelPhone: "31 98321-0000", labelEmail: "contact2@gmail.com", labelAddress: "Rua do Cristal, 22"))
        listaDeContatos.append(Contato(label1: "Contato 3", labelPhone: "31 98321-1111", labelEmail: "contact3@gmail.com", labelAddress: "Rua do Cristal, 33"))
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        let detalhesViewController = segue.destination as! DetalhesViewController
        let contato = sender as! Contato
        detalhesViewController.contato = contato

}
    
    extension ViewController: UITableViewDelegate{
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let contato = listaDeContatos[indexPath.row]
            performSegue(withIdentifier: "abrirDetalhes", sender: contato)
            
        }
    }
    
 


}
