//
//  DetalhesViewController.swift
//  Tela-Contact
//
//  Created by COTEMIG on 05/02/1444 AH.
//

import UIKit

class DetalhesViewController: UIViewController {
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var numero: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var endereco: UILabel!
    
    public var contato: Contato?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
            
        title = contato?.label1
        
        nome.text = contato?.label1
        numero.text = contato?.labelPhone
        email.text = contato?.labelEmail
        endereco.text = contato?.labelAddress
        
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
